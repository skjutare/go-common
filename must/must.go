package must

import (
	"encoding/json"
	"io/fs"
	"net/url"
	"os"
)

// Succeed checks the error and panic if err is not nil.
func Succeed(err error) {
	if err != nil {
		panic(err)
	}
}

// Return checks the error and panics if err is not nil otherwise returns the value t.
func Return[T any](t T, err error) T {
	Succeed(err)
	return t
}

// Json marshal any input to bytes, panics on err.
func Json(object any) []byte {
	return Return(json.Marshal(&object))
}

// ReadFile returns the content as bytes, panics on err.
func ReadFile(name string) []byte {
	return Return(os.ReadFile(name))
}

// WriteFile content to name file on disc, panics on err.
func WriteFile(name string, content []byte, perm fs.FileMode) {
	Succeed(os.WriteFile(name, content, perm))
}

// Url parses a raw url and returns a url.Url or panics on err.
func Url(rawURL string) *url.URL {
	parsed, err := url.Parse(rawURL)
	Succeed(err)
	return parsed
}
