package must

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type aJsonStruct struct {
	Key string `json:"key"`
}

func Test_must(t *testing.T) {

	t.Run("Must succeed", func(t *testing.T) {
		Succeed(nil)
	})

	t.Run("Must succeed panics when err", func(t *testing.T) {
		assert.Panics(t, func() {
			Succeed(errors.New("an error"))
		})
	})

	t.Run("Must bytes", func(t *testing.T) {
		expect := []byte{0, 1, 2, 5}
		res := Return(expect, nil)
		assert.Equal(t, expect, res)
	})

	t.Run("Must bytes panics when err", func(t *testing.T) {
		assert.Panics(t, func() {
			Return(make([]byte, 0), errors.New("an error"))
		})
	})

	t.Run("Must int", func(t *testing.T) {
		expect := 4
		res := Return(expect, nil)
		assert.Equal(t, expect, res)
	})

	t.Run("Must int panics when err", func(t *testing.T) {
		assert.Panics(t, func() {
			Return(4, errors.New("an error"))
		})
	})

	t.Run("Must Json panics when err", func(t *testing.T) {
		defer func() {
			if e := recover(); e == nil {
				t.FailNow()
			}
		}()

		bytes := Json(func() {})
		fmt.Println(string(bytes))
	})

	t.Run("Must json", func(t *testing.T) {
		expect := []byte("true")
		res := Json(true)
		assert.Equal(t, expect, res)
	})

	t.Run("Must json a struct", func(t *testing.T) {
		expect := []byte("{\"key\":\"value\"}")
		res := Json(aJsonStruct{
			Key: "value",
		})
		assert.Equal(t, expect, res)
	})

	t.Run("Must read file", func(t *testing.T) {
		tempFile, err := os.CreateTemp("", "tmp-file-*")
		assert.NoError(t, err)
		defer Succeed(os.Remove(tempFile.Name()))

		expected := []byte("some content")
		err = os.WriteFile(tempFile.Name(), expected, 0644)
		assert.NoError(t, err)

		Succeed(tempFile.Close())

		actual := ReadFile(tempFile.Name())

		assert.Equal(t, expected, actual)

	})

	t.Run("Must not read file", func(t *testing.T) {
		tempFile, err := os.CreateTemp("", "tmp-file-*")
		assert.NoError(t, err)
		err = os.Remove(tempFile.Name())
		assert.NoError(t, err)

		assert.Panics(t, func() {
			ReadFile(tempFile.Name())
		})
	})

	t.Run("Must write file on no error", func(t *testing.T) {

		tempFile, err := os.CreateTemp("", "tmp-file-*")
		assert.NoError(t, err)
		defer Succeed(os.Remove(tempFile.Name()))

		data := []byte("some content")

		WriteFile(tempFile.Name(), data, 0644)

		bytes, err := os.ReadFile(tempFile.Name())
		assert.NoError(t, err)
		assert.Equal(t, data, bytes)
	})

	t.Run("Must panic on write file with error", func(t *testing.T) {
		tempName, err := os.MkdirTemp("", "tmp-*")
		assert.NoError(t, err)

		defer os.RemoveAll(tempName)

		assert.Panics(t, func() {
			WriteFile(tempName, []byte("some content"), 0644)
		})
	})

	t.Run("Must parse valid url", func(t *testing.T) {
		url := Url("http://localhost")
		assert.Equal(t, "http://localhost", url.String())
	})

	t.Run("Must panic on a invalid url", func(t *testing.T) {
		assert.Panics(t, func() {
			_ = Url("http:\n//localhost")

		})
	})

}
