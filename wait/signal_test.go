package wait_test

import (
	"context"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/skjutare/go-common/wait"
)

func TestOsExitSignal(t *testing.T) {

	t.Run("Context cancellation", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 110*time.Millisecond)
		defer cancel()

		startTime := time.Now()
		err := wait.OsExitSignal(ctx)

		require.Error(t, err)

		duration := time.Since(startTime)
		require.True(t, duration >= 100*time.Millisecond, "Expected function to return after at least 100ms")
	})

	t.Run("OS signal", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		go func() {
			time.Sleep(110 * time.Millisecond)
			_ = syscall.Kill(syscall.Getpid(), syscall.SIGHUP)
		}()

		startTime := time.Now()
		err := wait.OsExitSignal(ctx)

		require.ErrorContains(t, err, "hangup")

		duration := time.Since(startTime)

		require.True(t, duration >= 100*time.Millisecond, "Expected function to return after at least 100ms")
	})

}
