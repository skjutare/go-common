package wait

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

// OsExitSignal Waits for SIGHUP, SIGINT, SIGTERM or SIGQUIT or context cancellation.
func OsExitSignal(ctx context.Context) error {
	ch := make(chan os.Signal, 1)

	signal.Notify(ch,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	select {
	case <-ctx.Done():
		return fmt.Errorf("exit on context done: %w", ctx.Err())

	case s := <-ch:
		return fmt.Errorf("os signal '%s' caused exit", s.String())
	}
}
